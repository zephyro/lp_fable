
$(document).ready(function() {

    $(".image-big-item a").fancybox({
        'padding'    : 0
    });

    $(".btn-modal-img").fancybox({
        'padding'    : 0
    });

    $(".btn-modal").fancybox({
        'padding'    : 0,
        'tpl'        : {
            closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>'
        }
    });

    $('.product-thumbs li').click(function (event) {
        event.preventDefault();
        var arr = '.' + $(this).attr("data-target");
        var box = $(this).closest('.product-gallery');
        box.find('.image-big-item').hide();
        box.find(arr).show();
        box.find('.product-thumbs li').removeClass('active');
        $(this).addClass('active');
    });

    var myMap;

    ymaps.ready(function () {
        myMap = new ymaps.Map('map', {
            zoom: 15,
            center: [59.93532000640847,30.315871722167973]
        }, {
            searchControlProvider: 'yandex#search'
        });
        var myPlacemark = new ymaps.Placemark([59.935470757924655,30.323081500000008], {
            balloonContent: '<div class="m_title">Приходите в наш шоу-рум<br/>в Санкт-Петербурге</div><div class="m_t1">10.00 до 21.00 без выходных.</div><div class="m_t2">191186, Санкт-Петербург, Невский пр., д.25, б/ц «Атриум на Невском,25», офис 301</div>'
        }, {
            balloonPanelMaxMapArea: 0
        });
        myMap.geoObjects.add(myPlacemark);


        myPlacemark.balloon.open();
    });




    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                type   =     $('input[name="type"]', $form).val(),
                name    =     $('input[name="name"]', $form).val(),
                phone   =     $('input[name="phone"]', $form).val(),
                email   =     $('input[name="email"]', $form).val(),
                time   =     $('input[name="time"]', $form).val();
            console.log(type, name, phone, email, home);
            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {type: type, name: name, phone: phone, email: email, time: time}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                document.location.href = "./thankyou.html";
            });
        }
    });

});

